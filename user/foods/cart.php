<?php
require_once __DIR__ . '/../../boot.php';
checkAuth('user');

$page_path = "/user/foods/cart.php";

$action = get('action');
$id = get('id');
$value = get('value');

$result = null;
switch ($action) {
    case 'update':
        $result = DB::update('cart', [
            'amount' => $value
        ], "`cart_id`='{$id}'");
        break;

    case 'delete':
        $result = DB::delete('cart', "`cart_id`='{$id}'");
        break;
}

if ($action) {
    redirect($page_path);
}

$items = DB::result("SELECT 
* ,
(`foods`.`food_price` * `cart`.`amount`) AS sum_price    
FROM `cart`
INNER JOIN `foods` ON `foods`.`food_id`=`cart`.`food_id`
WHERE `cart`.`user_id`='{$user_id}'");
ob_start();
?>
<?= showAlert() ?>
<h3>รายการอาหารในรถเข็น</h3>
<table>
    <thead>
        <tr>
            <th>รหัส</th>
            <th>ภาพอาหาร</th>
            <th>ชื่ออาหาร</th>
            <th>ราคาอาหาร</th>
            <th>จำนวน</th>
            <th>รวม</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($items as $item) : ?>
            <tr>
                <td><?= $item['food_id'] ?></td>
                <td>
                    <img src="<?= url($item['food_img']) ?>" alt="" style="
                        max-height: 8rem;
                    ">
                </td>
                <td><?= $item['food_name'] ?></td>
                <td><?= $item['food_price'] ?></td>
                <td><?= $item['amount'] ?></td>
                <td><?= $item['sum_price'] ?></td>
                <td>
                    <a href="?action=update&id=<?= $item['cart_id'] ?>&value=<?= intval($item['amount']) + 1 ?>">
                        เพิ่มจำนวน
                    </a>
                    <?php if ($item['amount'] !== '1') : ?>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="?action=update&id=<?= $item['cart_id'] ?>&value=<?= intval($item['amount']) - 1 ?>">
                            ลดจำนวน
                        </a>
                    <?php endif; ?> 
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="?action=delete&id=<?= $item['cart_id'] ?>">
                        ลบ
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php
$layout_page = ob_get_clean();
$page_name = 'รถเข็น';
require ROOT . '/user/layout.php';
