<?php

return [
    'app_name' => 'ระบบรับออเดอร์ร้านอาหาร',
    'site_url' => 'http://skill65.local/order_food',

    'db_host' => 'localhost',
    'db_user' => 'root',
    'db_password' => 'root',
    'db_name' => 'skill65_orderFood',
    'db_charset' => 'utf8mb4'
];
